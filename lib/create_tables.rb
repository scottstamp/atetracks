DB.create_table? :tracks do
  primary_key :id
  foreign_key :track_id
  blob :title
  blob :artist
  String :url
  String :track_type
  String :yt_id
end

DB.create_table? :mixes do
  primary_key :id
  blob :name
  String :web_path
  String :cover_url
  blob :author
  String :author_web_path
  blob :description, :size => 1000
  blob :description_html, :size => 1000
  String :likes_count
  String :plays_count
  String :tracks_count
  blob :tags
end

DB.create_table? :mixes_tracks do
  foreign_key :mix_id, :mixes, :null => false
  foreign_key :track_id, :tracks, :null => false
  primary_key [:mix_id, :track_id]
  index [:mix_id, :track_id]
end
