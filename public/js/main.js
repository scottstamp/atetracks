/*$(function(){
 $('a[href^="/"]').on('click', function(){
 $('body').removeAttr('style');
 $('body').addClass("loading");
 var ifr=$('<iframe></iframe>', {
 id:'MainPopupIframe',
 src:this.href,
 style:'display:none',
 load:function(){
 $(this).show();
 alert('iframe loaded !');
 }
 });
 location.href = this.href;
 });
 });*/

$(document).ready(function () {
    // Setup tag search field
    var tags = document.location.href.replace(/(.*)(popular|hot)\//g, '').replace(/\+/g, ',');
    if (~tags.indexOf("http://atetracks.ca/")) {
        tags = '';
    } else if (~tags.indexOf("/")) {
        tags = tags.replace('/', '');
    }

    $('#tokenfield').tokenfield({
        tokens: decodeURIComponent(tags)
    }).val(decodeURIComponent(tags));

    // Override default "submit" behavior.
    $('.form-inline').submit(function (e) {
        document.location.href = './' + $('#tokenfield').val().replace(/, /g, '+');
        return false;
    });
});