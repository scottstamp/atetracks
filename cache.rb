require 'sequel'
DB = Sequel.connect('do:mysql://atetracks:3f0GwIMUzn@localhost/atetracks')
require_relative 'lib/create_tables'
require_relative 'lib/models'

class Cache
  def is_cached?(mix_id = :mix_id)
    mix = Mix.first(:id => mix_id.to_i)
    unless mix.nil?
      (mix.tracks_dataset.count > 0)
    else
      false
    end
  end

  # BOM poisoning? Weird... stripped manually with gsub.
  # Sorry for the ugly code.
  def cache_mix(mix = :mix, tracks = :tracks)
    begin
      mix = Mix.create(
          :id => mix['id'].to_i,
          :name => mix['name'],
          :web_path => mix['web_path'],
          :cover_url => mix['cover_urls']['sq133'],
          :author => mix['user']['login'],
          :author_web_path => mix['user']['web_path'],
          :description => mix['description'],
          :tags => mix['tag_list_cache'],
          :likes_count => mix['likes_count'],
          :plays_count => mix['plays_count'],
          :tracks_count => mix['tracks_count']
      )
    rescue
      mix = Mix.create(
          :id => mix['id'].to_i,
          :name => mix['name'],
          :web_path => mix['web_path'],
          :cover_url => mix['cover_urls']['sq133'],
          :author => mix['user']['login'],
          :author_web_path => mix['user']['web_path'],
          :tags => mix['tag_list_cache'],
          :likes_count => mix['likes_count'],
          :plays_count => mix['plays_count'],
          :tracks_count => mix['tracks_count']
      )
    end

    tracks.each do |track|
      begin
        unless track['set'].nil?
          track = Track.create(
              :track_id => track['set']['track']['id'].to_i,
              :title => track['set']['track']['name'].gsub("\xEF\xBB\xBF", ''),
              :artist => track['set']['track']['performer'].gsub("\xEF\xBB\xBF", ''),
              :url => track['set']['track']['track_file_stream_url'],
              :track_type => track['set']['track']['stream_source'],
              :yt_id => track['set']['track']['yt_id']
          )
          mix.add_track(track)
        end
      rescue
        unless track['set'].nil?
          track = Track.create(
              :track_id => track['set']['track']['id'].to_i,
              :title => track['set']['track']['name'],
              :artist => track['set']['track']['performer'],
              :url => track['set']['track']['track_file_stream_url'],
              :track_type => track['set']['track']['stream_source'],
              :yt_id => track['set']['track']['yt_id']
          )
        end
      end
    end
  end

  def retrieve_cache(mix_id = :mix_id)
    Mix.first(:id => mix_id.to_i)
  end
end
