## ateTracks - Scott Stamp <scottstamp851@gmail.com>
require 'sinatra'
require './eighttracks.rb'

et = EightTracks.new

set :bind, '0.0.0.0'

get '/start_cache' do
  threads = []

  mixes = et.search('popular', '', 50)
  mixes = mixes + et.search('hot', '', 50)
  mixes.each do |mix|
    threads << Thread.new { et.request_playlist(mix['id']) }
  end

  threads.each do |thread|
    thread.join
  end
end

%w(/popular /hot).each do |path|
  get path do
    redirect "#{path}/"
  end
end

get '/popular/?:tags?/?' do
  @mixes = et.search('popular', params[:tags])
  erb :search
end

get '/hot/?:tags?/?' do
  @mixes = et.search('hot', params[:tags])
  erb :search
end

get '/favs/:user' do
  @mixes = et.favorites(params[:user])
  erb :search
end

get '/about/?' do
  erb :about
end

%w(/ /index /home).each do |path|
  get path do
    @popular = et.search('popular', '', '3')
    @hot = et.search('hot', '', '3')
    erb :index
  end
end

get '/playlist/:id/?' do
  attachment("#{params[:id]}.m3u")
  et.parse_playlist_m3u(et.request_playlist(params[:id]).tracks)
end

get '/yt/:id/?' do
  et.parse_playlist_yt(et.request_playlist(params[:id]).tracks)
end

get '/raw/:id/?' do
  et.parse_playlist_raw(et.request_playlist(params[:id]).tracks)
end

get '/:id/?' do
  @mix = et.request_playlist(params[:id])
  erb :view_mix, :locals => {
      :renderedTracks => et.parse_playlist(@mix.tracks)
  }
end
