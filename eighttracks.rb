require 'httparty'
require 'cgi'
require './cache.rb'
require 'htmlentities'

class EightTracks
  @token = ''
  include HTTParty
  base_uri '8tracks.com'
  format :json

  def initialize
    @token = self.class.get('/sets/new?format=jsonh')['play_token']
  end

  def coder(string = :string)
    entities = HTMLEntities.new
    entities.encode(string.force_encoding('utf-8'), :basic)
  end

  def renew_token
    @token = self.class.get('/sets/new?format=jsonh')['play_token']
  end

  def search(type = :type, tags = '', num = '50')
    unless tags.nil?
      tags = CGI.escape(tags)
      tags.downcase!
    end
    self.class.get("/mixes?sort=#{type}&tag=#{tags}&per_page=#{num}&include=mixes&format=jsonh")['mix_set']['mixes']
  end

  def favorites(user = :user, num = '500')
    self.class.get("/mix_sets/liked:#{user}?include=mixes&per_page=500&format=jsonh")['mix_set']['mixes']
  end

  def get_mix_data(mix_id = :mix_id)
    self.class.get("/mixes/#{mix_id}?format=jsonh")
  end

  def determine_type(track = :track)
    case track
      when 'match_sc'
        'mp3'
      when 'ext_sc'
        'mp3'
      when 'upload_v3'
        'm4a'
      else
        'm4a'
    end
  end

  def request_playlist(mix_id = :mix_id)
    cache = Cache.new
    if cache.is_cached? mix_id
      cache.retrieve_cache(mix_id)
    else
      renew_token
      tracks = [request_track(mix_id, 'play')]
      next_track = request_track(mix_id, 'next')
      until next_track['set']['at_end']
        tracks = tracks + [next_track]
        begin
          next_track = request_track(mix_id, 'next')
        rescue
          sleep(1)
          next_track = request_track(mix_id, 'next')
        end
      end
      cache.cache_mix(get_mix_data(mix_id)['mix'], tracks)
      cache.retrieve_cache(mix_id)
    end
  end

  def parse_playlist(tracks = :tracks)
    parsed = ''
    tracks.each_with_index do |track, index|
      type = determine_type(track[:track_type])
      name = coder(track[:title])
      performer = coder(track[:artist])
      url = track[:url]
      lend = ','
      if index == tracks.size - 1
        lend = ''
      end
      parsed << "{ free: true, title: '#{name} - #{performer}', #{type}: '#{url}' }#{lend}"
    end
    parsed
  end

  def parse_playlist_m3u(tracks = :tracks)
    parsed = "#EXTM3U\r\n\r\n"
    tracks.each do |track|
      name = track[:title].strip
      performer = track[:artist].strip
      url = track[:url].strip
      parsed << "#EXTINF:0,#{performer} - #{name}\r\n#{url}\r\n"
    end
    parsed
  end

  def parse_playlist_raw(tracks = :tracks)
    parsed = ''
    tracks.each do |track|
      parsed << track[:url].strip + "\r\n"
    end
    parsed
  end

  def parse_playlist_yt(tracks = :tracks)
    parsed = ''
    tracks.each do |track|
      parsed << "<a href='http://www.youtube.com/v/#{track['yt_id']}'>#{track['url']}</a><br />"
    end
    parsed
  end

  def request_track(id = :id, first = :first)
    self.class.get("/sets/#{@token}/#{first}?mix_id=#{id}&include=track%5Byoutube%5D&format=jsonh")
  end

  def get_popular(tags = :tags)
    self.class.get("/mixes?tag=#{tags}&format=jsonh&sort=popular&include=mixes")
  end
end
